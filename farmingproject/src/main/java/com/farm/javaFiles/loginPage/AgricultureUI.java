package com.farm.javaFiles.loginPage;



import com.farm.javaFiles.Screens.HomePage;

import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AgricultureUI extends Application{

    

    @Override
    public void start(Stage stage) throws Exception {

         
        Image backgroundImage = new Image("assets/images/AgricultureUI/background.jpeg");
        ImageView backgroundImageView  = new ImageView(backgroundImage);
        backgroundImageView.setFitHeight(900);
        backgroundImageView.setFitWidth(1480);
        
        Image topImage = new Image("assets/images/AgricultureUI/topbar.jpeg");
        ImageView topImageView  = new ImageView(topImage);
        topImageView.setFitHeight(120);
        topImageView.setFitWidth(1480);
        
        
        Image krushi = new Image("assets/images/AgricultureUI/hellokrushi.png");
        ImageView krushImageView  = new ImageView(krushi);
        krushImageView.setFitHeight(70);
        krushImageView.setFitWidth(150);

        Label homeLabel = getL("Home");
        Label aboutLabel = getL("About");
        Label pagesLabel = getL("Pages");
        Label projectLabel = getL("Project");
        Label newsLabel = getL("News");

        HBox navigationHBox = new HBox(45);
        navigationHBox.setAlignment(Pos.CENTER);
        navigationHBox.getChildren().addAll(homeLabel,aboutLabel,pagesLabel,projectLabel,newsLabel);

        Image callImage=new Image("assets/images/AgricultureUI/callIcon.png");
        ImageView callImageView  = new ImageView(callImage);
        callImageView.setFitHeight(20);
        callImageView.setFitWidth(20);

        // Label callLabel=new Label("Call Anytime");
        // callLabel.setStyle("-fx-text-fill: white");
        // callLabel.setFont(new Font(15));

        // Label mobNumber=new Label("0123456789");
        // mobNumber.setFont(new Font(15));
        // mobNumber.setStyle("-fx-text-fill: white");

        
    //     VBox contactBox=new VBox();
    //     contactBox.getChildren().addAll(callLabel,mobNumber);
    //    contactBox.setStyle("-fx-background-color:aqua");
    //     contactBox.setPrefHeight(10);
        

        Label callLabel = new Label("Call Anytime\n0123456789");
        callLabel.setStyle("-fx-text-fill: white");
        
        HBox callHBox=new HBox();
        callHBox.getChildren().addAll(callImageView,callLabel);
        callHBox.setStyle("-fx-background-color: #ffc107; -fx-background-radius:5; ");
        callHBox.setPrefHeight(30);

        
        
        // HBox contactBox=new HBox(7);
        // contactBox.setAlignment(Pos.CENTER);
        // contactBox.getChildren().addAll(callImageView);
        // contactBox.setStyle("-fx-background-color: #ffc107; -fx-background-radius:5");

        HBox navHBox=new HBox(250);
        navHBox.getChildren().addAll(krushImageView,navigationHBox,callHBox);
        navHBox.setPadding(new Insets(10,0,0,100));

        BorderPane topBar=new BorderPane();
        topBar.setTop(navHBox);
        topBar.setPadding(new Insets(20,0,0,0));

       

        Label mainLabel=new Label("WELCOME TO AGRICULTURE FARM");
        mainLabel.setFont(new Font(15));
        mainLabel.setStyle("-fx-text-fill:white; -fx-font-weight:BOLD");
        mainLabel.setTextFill(Color.WHITE);

        Label agriLabel=new Label("Agriculture\n& Eco Farming");
        agriLabel.setFont(Font.font("Georgia", 35));
        agriLabel.setStyle("-fx-text-fill:white");
        agriLabel.setTextFill(Color.WHITE);

        Label descriptionLabel=new Label("There are many of passeges of lorem lpsum,but the majori\nhas suffered with alteration in some form");
        descriptionLabel.setFont(new Font(15));
        descriptionLabel.setStyle("-fx-text-fill:white");
        descriptionLabel.setTextFill(Color.WHITE);

        Button loginBt = new Button("Login");
        loginBt.setStyle("-fx-background-color: #ffc107; -fx-text-fill: white; -fx-font-weight:BOLD");
        loginBt.setFont(new Font("Georgia", 20));
        loginBt.setPrefWidth(120);

        // loginBt.setOnMouseEntered(e->{
        //     loginBt.setScaleX(1.2);
        //     loginBt.setScaleY(1.2);
        // });

        // loginBt.setOnMouseExited(e->{
            
        // });


      

        Button signupButton = new Button("Signup");
        signupButton.setStyle("-fx-background-color: #ffc107; -fx-text-fill: white; -fx-font-weight:BOLD");
        signupButton.setFont(new Font("Arial", 20));
        signupButton.setPrefWidth(120);


        HBox loginHBox=new HBox(45,loginBt,signupButton);
        loginHBox.setPadding(new Insets(575,0,0,140));

        VBox mainVBox=new VBox(15);
        mainVBox.getChildren().addAll(mainLabel,agriLabel,descriptionLabel);
        mainVBox.setPadding(new Insets(350,0,0,140));

        // TranslateTransition slide=new TranslateTransition();
        // EmailLoginPage obj=new EmailLoginPage();
        // VBox loginFieldBox=obj.login(new HomePage(stage));
        // slide.setDuration(Duration.seconds(3));
        // slide.setNode(loginFieldBox);
        // slide.setFromY(600);
        // slide.setToY(300);
        // slide.setCycleCount(1);
        // slide.setAutoReverse(true);
        

        StackPane agriculturStackPane=new StackPane();
        agriculturStackPane.getChildren().addAll(backgroundImageView,topImageView,topBar,mainVBox,loginHBox);
        agriculturStackPane.setAlignment(topImageView, Pos.TOP_CENTER);

        Group startPageGroup  = new Group(agriculturStackPane);
        Scene startScene = new Scene(startPageGroup);
        stage.setScene(startScene);
        stage.setTitle("LoginPage");
        stage.setHeight(900);
        stage.setWidth(1470);
        stage.setX(0);
        stage.setY(0);
        stage.show();
    }

     Label getL(String name){
        Label lb = new Label(name);
        lb.setTextFill(Color.GREY);
        lb.setFont(Font.font("Calibri", 18));
        return lb ;
    }

}
