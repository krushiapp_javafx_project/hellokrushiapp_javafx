package com.farm.javaFiles.loginPage;

import java.io.FileInputStream;
import java.io.IOException;

import com.farm.controller.LoginController;
import com.farm.firebase_connection.FirebaseService;
import com.farm.firebase_connection.FirebaseService1;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.farm.javaFiles.Screens.HomePage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class EmailLoginPage {

   // int flag1 =0 ;

     TextField emailField ;
    PasswordField passwordField ;
    Image backgroundImage ;
    ImageView backgroundImageView ;
    Button  continueBotton;
    Image arrowImage;
    ImageView arrowImageView ;
    Button arrow;
    VBox startPageVBox;
    StackPane startPageStackPane;
    FirebaseService1 firebaseService1;

    Stage stage ;

    public  EmailLoginPage(Stage stage){
      
        this.stage = stage ;
    }

   public  void login(HomePage obj1){


        FirebaseService1 obj = new  FirebaseService1(this, emailField, passwordField);

    
        // this.flag1 = obj.flag;

      //  System.out.println(flag1);

        emailField = new TextField() ;
        emailField.setMaxWidth(250);
        emailField.setPromptText("Enter your email...");

        passwordField = new PasswordField();
        passwordField.setMaxWidth(250);
        passwordField.setPromptText("Enter Password");

        backgroundImage = new Image("assets/images/loginpage/backgroundImage.jpeg");
        backgroundImageView = new ImageView(backgroundImage);
        backgroundImageView.setFitHeight(920);
        backgroundImageView.setFitWidth(600);
        backgroundImageView.setLayoutX(0);

     

        firebaseService1 = new FirebaseService1(this,emailField,passwordField);



        continueBotton= new Button("Continue");
        continueBotton.setStyle("-fx-text-fill: white");
        continueBotton.setStyle("-fx-background-Color:green;-fx-text-fill:white;-fx-background-radius:15");
        continueBotton.setMaxWidth(200);



      continueBotton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

            firebaseService1.continuemethod(obj1);
              
            }
            
        });


        arrowImage = new Image("assets/images/screensImage/goback.png");
        arrowImageView = new ImageView(arrowImage);
        arrowImageView.setFitHeight(20);
        arrowImageView.setFitWidth(20);

        arrow=new Button();
        arrow.setGraphic(arrowImageView);
        arrow.setOnAction(new  EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
             
             
                StartPage obj = new StartPage(stage);
                obj.start();
        
                
            }
            
        });



         startPageVBox = new VBox(30);
        startPageVBox.getChildren().addAll(arrow , emailField , passwordField , continueBotton);
        startPageVBox.setAlignment(Pos.CENTER);
        startPageVBox.setLayoutX(350);
        

        startPageStackPane = new StackPane(backgroundImageView,startPageVBox);
        startPageStackPane.setLayoutX(450);
        startPageStackPane.setAlignment(Pos.CENTER);




        Group startPageGroup  = new Group(startPageStackPane);
        Scene startScene = new Scene(startPageGroup);


        
       
        stage.setScene(startScene);
        stage.setTitle("LoginPage");
        stage.setHeight(900);
        stage.setWidth(1470);
        stage.setX(0);
        stage.setY(0);
        stage.show();







    }
    
}
