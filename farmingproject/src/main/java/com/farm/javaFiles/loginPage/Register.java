package com.farm.javaFiles.loginPage;

import com.farm.firebase_connection.FirebaseService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.farm.javaFiles.Screens.HomePage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import javafx.stage.Stage;

public class Register {
   
     VBox startPageVBox  ;
    Image frontImage ;
      Image backgroundImage ;
    ImageView frontImageView ;
    ImageView backgroundImageView ;
    Button signUpButton ;
    Button loginButton ; 
    Button continueBotton ;
    TextField emailField ;
    PasswordField passwordField;
    FirebaseService firebaseService;

    Stage stage ;

    Register(Stage stage){

        this.stage = stage ;

    }

    void register(){



        backgroundImage = new Image("assets/images/loginpage/backgroundImage.jpeg");
        backgroundImageView = new ImageView(backgroundImage);
        backgroundImageView.setFitHeight(920);
        backgroundImageView.setFitWidth(600);
        backgroundImageView.setLayoutX(0);

        Image hellokrushi = new Image("assets/images/loginpage/hellokrushi.png");
        ImageView helloKruImageView = new ImageView(hellokrushi);
        helloKruImageView.setFitHeight(130);
        helloKruImageView.setFitWidth(180);


        Label regiLabel = new Label("Do Register !");
        regiLabel.setFont(new Font("Arial", 20));
        regiLabel.setTextFill(Color.GREEN);
        regiLabel.setStyle("-fx-font-weight:BOLD");
        regiLabel.setLayoutX(100);


        Image personImage = new Image("assets/images/loginpage/picon.png");
        ImageView personImageView = new ImageView(personImage);
        personImageView.setFitHeight(25);
        personImageView.setFitWidth(25);

        TextField name = new TextField();
        name.setPromptText("Enter your name");
        name.setPrefWidth(250);

        HBox nameBox = new HBox(10,personImageView , name);
         

        Image ageImage = new Image("assets/images/loginpage/ages.png");
        ImageView ageImageView = new ImageView(ageImage);
        ageImageView.setFitHeight(25);
        ageImageView.setFitWidth(25);
        


        TextField age = new TextField();
        age.setPromptText("Enter your age");
        age.setPrefWidth(250);

        HBox ageBox = new HBox(10,ageImageView , age);

        Image emailImage = new Image("assets/images/loginpage/email.png");
        ImageView emailImageView = new ImageView(emailImage);
        emailImageView.setFitHeight(25);
        emailImageView.setFitWidth(25);
        // emailImageView.setPreserveRatio(true);

        emailField = new TextField() ;
        emailField.setPrefWidth(250);
        emailField.setPromptText("Enter your email...");

        HBox emailBox = new HBox(10,emailImageView , emailField);

        passwordField = new PasswordField();
        passwordField.setPrefWidth(250);
        passwordField.setPromptText("Enter Password");

        Image passwordImage = new Image("assets/images/loginpage/password.png");
        ImageView passwordImageView = new ImageView(passwordImage);
        passwordImageView.setFitHeight(25);
        passwordImageView.setFitWidth(25);


        HBox passwordBox = new HBox(10,passwordImageView , passwordField);





        RadioButton maleRadioButton = new RadioButton("Male");
        RadioButton femaleRadioButton = new RadioButton("Female");
        RadioButton otherRadioButton = new RadioButton("Other");

        ToggleGroup genderToggleGroup = new ToggleGroup();
        maleRadioButton.setToggleGroup(genderToggleGroup);
        femaleRadioButton.setToggleGroup(genderToggleGroup);
        otherRadioButton.setToggleGroup(genderToggleGroup);

        HBox genderBox = new HBox(30,maleRadioButton, femaleRadioButton, otherRadioButton);



        Label chooseType = getL("Choose type");

        ComboBox<String> farmerTypecomboBox = new ComboBox<>();
        farmerTypecomboBox.getItems().addAll(
                "शेतकरी",
                "शेतमाल व्यापारी",
                "थेट ग्राहक",
                "पशू व्यापारी"
        );
        farmerTypecomboBox.setPromptText("तुम्ही कोण आहात ते निवडा..?");
        farmerTypecomboBox.setPrefWidth(280);
        VBox farmerTypeVBox=new VBox(5,chooseType , farmerTypecomboBox);
        



        Label chooseDiv = getL("Choose Division");
         
        ComboBox<String> divTypecomboBox = new ComboBox<>();
         divTypecomboBox.getItems().addAll(

                "शेतकरी",
                "शेतमाल व्यापारी",
                "थेट ग्राहक",
                "पशू व्यापारी"
        );
        divTypecomboBox.setPromptText("कृपया विभाग निवडा .!");
        divTypecomboBox.setPrefWidth(280);
        VBox divTypeVBox=new VBox(5,chooseDiv ,divTypecomboBox);
        


        Label chooseDis = getL("Choose District");
         
        ComboBox<String> disTypecomboBox = new ComboBox<>();
         disTypecomboBox.getItems().addAll(

                "शेतकरी",
                "शेतमाल व्यापारी",
                "थेट ग्राहक",
                "पशू व्यापारी"
        );
        disTypecomboBox.setPromptText("कृपया जिल्हा निवडा .!");
        disTypecomboBox.setPrefWidth(280);
        VBox disTypeVBox=new VBox(5,chooseDis ,disTypecomboBox);
        

        Button regButton = new Button("Register Now... ");
        regButton.setStyle("-fx-background-color: GREEN ; -fx-background-radius:15");
        regButton.setTextFill(Color.WHITE);
        regButton.setPrefWidth(130);






        firebaseService = new FirebaseService(this,emailField,passwordField);



        regButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                System.out.println("error - Please enter the details!!");
                firebaseService.registerMethod();
               
            }
            
        });
        
        



        VBox regVBox = new VBox(25,nameBox ,ageBox ,emailBox,passwordBox,genderBox , farmerTypeVBox,divTypeVBox,disTypeVBox ) ; 
        regVBox.setPrefWidth(320);
        

         
        Button arrow=new Button("Go Back");
        arrow.setStyle("-fx-background-color:GREEN ; -fx-background-radius:15");
        arrow.setTextFill(Color.WHITE);
        arrow.setPrefWidth(70);
        arrow.setLayoutX(580);
        arrow.setLayoutY(90);
        arrow.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
             
             
                StartPage obj = new StartPage(stage);
                obj.start();
        
                
            }
            
        });

        

        VBox upperVBox=new VBox( 25,helloKruImageView,regiLabel,regVBox,regButton);
        upperVBox.setAlignment(Pos.CENTER);
        upperVBox.setLayoutY(70);
        upperVBox.setLayoutX(630);
       
       
        




        // StackPane registerStackPane = new StackPane();
        // registerStackPane.setLayoutX(450);
        // registerStackPane.setAlignment(Pos.CENTER);


       



        Group startPageGroup  = new Group(arrow,upperVBox);
        Scene startScene = new Scene(startPageGroup);


        
       
        stage.setScene(startScene);
        stage.setTitle("LoginPage");
        stage.setHeight(900);
        stage.setWidth(1470);
        stage.setX(0);
        stage.setY(0);
        stage.show();
       
    }

    Label getL(String name){
        Label lb = new Label(name);
        lb.setTextFill(Color.GREEN);
        lb.setFont(Font.font("Arial", 10));
        return lb ;
    }
    
    
}
