package com.farm.javaFiles.Screens;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.farm.javaFiles.functions.AppBar;
import com.farm.javaFiles.functions.SearchAppBar;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class AnimalHusbandry {

    Stage stage ;
    HomePage obj ;


   

   public AnimalHusbandry(Stage stage ){
        this.stage = stage ;
       
       
    }

    public  void getAnimalsScreen(){



        SearchAppBar searchObj = new SearchAppBar(stage);

        BorderPane animalBorderPane =   searchObj.getToolBar();



        






       
        ImageView imgbhail = takeImage(new Image("assets/images/animalshusbandary/bhail.jpeg"));
        Label bhail = getNameLabel("बैल");
        Label rupeeBhaiLabel  = getPriceLabel("₹60000");
        Label villagLabel = getContactLabel("गाव.तरंगफळ");


        VBox bhailVBox = getVBox(imgbhail, bhail, rupeeBhaiLabel, villagLabel);


       

        ImageView buffaloImageView = takeImage(new Image("assets/images/animalshusbandary/buffalo.jpeg"));
        Label buffaloLabel = getNameLabel("म्हैस");
        Label rupeebuffaloLabel = getPriceLabel("₹90000");
        Label buffalovillagLabel = getContactLabel("गाव.परभणी");

        VBox buffaloVBox = getVBox(buffaloImageView, buffaloLabel, rupeebuffaloLabel, buffalovillagLabel);



        ImageView vaasruImageView = takeImage(new Image("assets/images/animalshusbandary/kalvad.jpeg"));
        Label vaasruLabel = getNameLabel("वासरू");
        Label rupeevaasruLabel = getPriceLabel("₹2000");
        Label vaasruvillagLabel = getContactLabel("गाव.लातूर");

        VBox vaasruVBox = getVBox(vaasruImageView, vaasruLabel, rupeevaasruLabel, vaasruvillagLabel);


        HBox hb1 = new HBox(150,bhailVBox,buffaloVBox,vaasruVBox);
        hb1.setPadding(new Insets(0,0,0,100));


        ImageView mendhaImageView = takeImage(new Image("assets/images/animalshusbandary/mendha.jpeg"));
        Label mendhLabel = getNameLabel("शेळी");
        Label rupeeMendhLabel = getPriceLabel("₹11000");
        Label mendhavillagLabel = getContactLabel("गाव.जालना");

        VBox  mendhaVBox = getVBox(mendhaImageView, mendhLabel, rupeeMendhLabel, mendhavillagLabel);

        ImageView zaframendhaImageView = takeImage(new Image("assets/images/animalshusbandary/zafra.jpeg"));
        Label zafraLabel = getNameLabel("शेळी");
        Label rupeezafraLabel = getPriceLabel("₹11000");
        Label zafravillagLabel = getContactLabel("गाव.जालना");

        VBox  zafraVBox = getVBox(zaframendhaImageView, zafraLabel, rupeezafraLabel, zafravillagLabel);

        ImageView kalvadamendhaImageView = takeImage(new Image("assets/images/animalshusbandary/mhais.jpeg"));
        Label kalvadLabel = getNameLabel("वासरू");
        Label rupeekalvadLabel = getPriceLabel("₹1000");
        Label kalvadvillagLabel = getContactLabel("गाव.जालना");

        VBox  kalvadVBox = getVBox(kalvadamendhaImageView, kalvadLabel, rupeekalvadLabel, kalvadvillagLabel);


        HBox hb2 = new HBox(150,mendhaVBox,zafraVBox,kalvadVBox);
        hb2.setPadding(new Insets(0,0,0,100));

       


        
        
        VBox finalVBox = new VBox(50,hb1,hb2);
        // finalVBox.setLayoutX(450);
        finalVBox.setPadding(new Insets(10,0,0,0));


       
        // Wrap the VBox in a ScrollPane
        ScrollPane scrollPane = new ScrollPane(finalVBox);
        scrollPane.setPadding(new Insets(10));
        
        // Optional: Customize scroll pane properties
        scrollPane.setFitToWidth(true); // Ensure content width fits the ScrollPane width
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide horizontal scrollbar if not needed
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED); // Show vertical scrollbar as needed

        VBox vb = new VBox(animalBorderPane,scrollPane);

        
       



        // Group gp = new Group();


        Scene scene = new Scene(vb);
        stage.setScene(scene);
        stage.setTitle("LoginPage");
        stage.setHeight(900);
        stage.setWidth(1470);
        stage.setX(0);
        stage.setY(0);
        stage.show();



        



    }

     Label getNameLabel(String name){
        Label lb = new Label(name);
        lb.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        // lb.setMaxWidth(270);
        // lb.setPrefHeight(40);
        // lb.setPadding(new Insets(0,0,0,45));
        lb.setAlignment(Pos.CENTER);


        return lb ;
    }

    

    Label getPriceLabel(String name){
        Label lb = new Label(name);
        lb.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        lb.setStyle("-fx-text-fill: green");
        // lb.setMaxWidth(270);
        // lb.setPrefHeight(40);
        // lb.setPadding(new Insets(0,0,0,90));
        lb.setAlignment(Pos.CENTER);


        return lb ;
    }

    Label getContactLabel(String name){
        Label lb = new Label(name);
        lb.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        // lb.setMaxWidth(270);
        // lb.setPrefHeight(40);
        // lb.setPadding(new Insets(0,0,0,75));
        lb.setAlignment(Pos.CENTER);
        lb.setStyle("-fx-background-color: green; -fx-padding: 10;-fx-background-radius: 10 ;-fx-border-radius: 10;-FX-TEXT-FILL:WHITE");
        
        

        return lb ;
    }



    public static ImageView takeImage(Image image){
        ImageView img = new ImageView(image);
        img.setFitWidth(220);
        img.setFitHeight(200);
        img.setStyle("-fx-background-color: white; -fx-border-color: transparent");

        Rectangle clip = new Rectangle(210, 190);
        clip.setArcWidth(20);
        clip.setArcHeight(20);
        img.setClip(clip);

      
        return img ;
    }


    public static VBox getVBox(ImageView iv , Label lb,Label lb1 , Label lb2 ){
        VBox vb = new VBox(iv, lb ,lb1 ,lb2);
        vb.setSpacing(10);
        // vb.setAlignment(Pos.CENTER);
        vb.setStyle("-fx-background-color:#FAF9F6;-fx-border-color: green;-fx-border-width: 2;-fx-border-radius: 10;-fx-background-radius: 10;");
        vb.setPadding(new Insets(25));
        vb.setPrefWidth(210);
        vb.setAlignment(Pos.CENTER);
        return vb ;
    }

    
}
