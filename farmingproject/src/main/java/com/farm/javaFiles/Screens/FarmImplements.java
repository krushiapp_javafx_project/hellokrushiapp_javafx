package com.farm.javaFiles.Screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.farm.javaFiles.functions.AppBar;


import javafx.geometry.Insets;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class FarmImplements {


     List<Map<String, String>> animalData = new ArrayList<Map<String, String>>() {{
            // First map
            add(new HashMap<String, String>() {{
                put("image", "assets/images/farmingImplements/Kisan Kraft 6 hp Diesel Inter Cultivator Tiller KK-IC-256D.jpg");
                put("name", "16 HP Suraj Tractor,\n            2WD");
                put("price", "300000");
                put("contact","9765872086");

            }});
            add(new HashMap<String, String>() {{
                put("image", "assets/images/farmingImplements/Cultivator.jpeg");
                put("name", "6-8 Feet Agriculture\n Tractor Cultivator");
                put("price", "350000");
                put("contact","9765872086");
            }});
            add(new HashMap<String, String>() {{
                put("image", "assets/images/farmingImplements/CombineHarvester.jpg");
                put("name", "CombineHarvester  \n    ");
                put("price", "300000");
                put("contact","9765872086");
            }});
            add(new HashMap<String, String>() {{
                put("image", "assets/images/farmingImplements/Harrow.jpeg");
                put("name", "Harrow\n   ");
                put("price", "300000");
                put("contact","9765872086");
            }});
            add(new HashMap<String, String>() {{
                put("image", "assets/images/farmingImplements/PowerHarrow.png");
                put("name", "IPowerHarrow \n ");
                put("price", "300000");
                put("contact","9765872086");
            }});
            add(new HashMap<String, String>() {{
                put("image", "assets/images/farmingImplements/Sprayer.jpg");
                put("name", "Sprayer \n ");
                put("price", "300000");
                put("contact","9765872086");
            }});
        }};
    

    Stage stage;

    public  FarmImplements(Stage stage){

        this.stage =stage;

    }  

    public   void getfarmImplements(){


        // SearchAppBar searchObj = new SearchAppBar(stage);
        // BorderPane farmBorderPane =   searchObj.getToolBar();


        AppBar appbarObj  = new AppBar(stage);
        BorderPane appBar = appbarObj.getToolBar();


        VBox finalVBox = new VBox(50,appBar);
        // finalVBox.setLayoutX(450);
        finalVBox.setPadding(new Insets(0,0,0,50));


        
        

        AnimalHusbandry obj = new AnimalHusbandry(stage);


        GridPane gp = new GridPane();
        gp.setHgap(60);
        gp.setVgap(60);
        gp.setPadding(new Insets(50,70,50,80));
        gp.setStyle("-fx-background-color:LIGHTGRAY");


        int num = 0;
        for(int row = 0 ; row<animalData.size()/4; row++){

            for(int col = 0 ; col<4 ; col++){

                ImageView kalvadamendhaImageView = obj.takeImage(new Image(animalData.get(num).get("image")));
                Label kalvadLabel = obj.getNameLabel(animalData.get(num).get("name"));
                Label rupeekalvadLabel = obj.getPriceLabel(animalData.get(num).get("price"));
                Label contactLabel = obj.getContactLabel((animalData.get(num).get("contact")));

              //  VBox  animalVBox = new VBox(kalvadamendhaImageView, kalvadLabel, rupeekalvadLabel);
              VBox animalVBox = obj.getVBox(kalvadamendhaImageView, kalvadLabel, rupeekalvadLabel,contactLabel);

                gp.add(animalVBox,col,row );

                num++;
               
            }

        }
       



        ScrollPane scrollPane = new ScrollPane(gp);
        scrollPane.setPadding(new Insets(10));
        
        // Optional: Customize scroll pane properties
        scrollPane.setFitToWidth(true); // Ensure content width fits the ScrollPane width
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide horizontal scrollbar if not needed
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED); // Show vertical scrollbar as needed

        VBox vb = new VBox(finalVBox,scrollPane);







       



        // Group gp = new Group(farmBorderPane);
        Scene scene = new Scene(vb);
        scene.setFill(Color.LIGHTGRAY);
        stage.setScene(scene);
        stage.setTitle("LoginPage");
        stage.setHeight(900);
        stage.setWidth(1470);
        stage.setX(0);
        stage.setY(0);
        stage.show();


    }



    
    
}
