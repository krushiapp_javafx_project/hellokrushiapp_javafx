package com.farm.javaFiles.functions;

import com.farm.javaFiles.Screens.HomePage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.ImageCursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;



// ya class madhun appbar return hotoy

public class AppBar {
    Stage stage ;

    public  AppBar(Stage stage){
        this.stage = stage ;
    }


   public BorderPane getToolBar(){
        ToolBar appBar = new ToolBar();


        Image krushi = new Image("assets/images/screensImage/helloKrushi.png");
        ImageView krushImageView  = new ImageView(krushi);
        krushImageView.setFitHeight(60);
        krushImageView.setFitWidth(120);



        Label homeLabel = getL("Home");
        Label aboutLabel = getL("About");
        Label pagesLabel = getL("Pages");
        Label projectLabel = getL("Project");
        Label newsLabel = getL("News");

        HBox navigationHBox = new HBox(55);
        navigationHBox.setAlignment(Pos.CENTER);
        navigationHBox.getChildren().addAll(homeLabel,aboutLabel,pagesLabel,projectLabel,newsLabel);
   


        //   ImageView personImageView = getImage(new Image("assets/images/screensImage/person.png"));
        //   personImageView.setFitHeight(35);
        //   personImageView.setFitWidth(40);
        //   Button persButton = getButton(personImageView);

        //   persButton.setOnAction( new EventHandler<ActionEvent>(){

        //     @Override
        //     public void handle(ActionEvent event) {

        //         System.out.println("hello in person");
                
        //     }

        //   });


        //   ImageView savedImageView = getImage(new Image("assets/images/screensImage/saved.png"));
        //   Button savedButton = getButton(savedImageView);

        //   ImageView notificationImageView = getImage(new Image("assets/images/screensImage/notification.png"));
        //   Button notificationButton = getButton(notificationImageView);

        //   HBox threeIconHBox = new HBox(10 ,persButton , savedButton , notificationButton);


        HBox mainHBox = new HBox(450, krushImageView , navigationHBox);

        appBar.getItems().addAll(mainHBox);
        appBar.setStyle("-fx-background-color: transparent;");

        BorderPane root = new BorderPane();
        root.setTop(appBar);
        root.setPadding(new Insets(15, 0, 0, 50));

           

        return root ;
          


    }
    ImageView getImage(Image img){
        ImageView image = new ImageView(img);
        image.setFitHeight(30);
        image.setFitWidth(30);
        return image ;
    }

    Button getButton(ImageView iv){
        Button bt = new Button();
        bt.setGraphic(iv);
        bt.setStyle("-fx-background-color: transparent; -fx-border-color: transparent");

        return bt ;
    }
     Label getL(String name){
        Label lb = new Label(name);
        lb.setTextFill(Color.GREY);
        lb.setFont(Font.font("Calibri", 18));
        return lb ;
    }

}
