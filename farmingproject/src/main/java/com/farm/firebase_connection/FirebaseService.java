package com.farm.firebase_connection;



import com.farm.controller.LoginController;
import com.farm.javaFiles.loginPage.EmailLoginPage;
import com.farm.javaFiles.loginPage.Register;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;

import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class FirebaseService {

    Stage stage;
    
    

    TextField emailField;
    EmailLoginPage emailLoginPage;
    
    PasswordField passwordField;
    LoginController loginController;
    Register register;








    public FirebaseService(Register register ,TextField emailField,PasswordField passwordField){
        // this.emailLoginPage = emailLoginPage;
        this.register = register;

        this.emailField =emailField;
     
        this.passwordField=passwordField;

    }

    public void registerMethod(){
       try {
           
            // Create user request
            String email =  emailField.getText();
            String password = passwordField.getText();

            UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                    .setEmail(email)
                    .setPassword(password)
                    .setDisabled(false);

                 



            // Create user in Firebase Auth

            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully created user: " + userRecord.getUid());
            showAlert("Success", "User created successfully."+"\nUserId : "+userRecord.getUid()+"\nUserEmailId : "+userRecord.getEmail()+"\n Please Rember the Password!!");

        } catch (FirebaseAuthException e) {
            showAlert("Error", "Failed to create user: " + e.getMessage());
            e.printStackTrace();
        }
    }


   

    void showAlert(String title, String message) {
        // Alert alert = new Alert(Alert.AlertType.INFORMATION);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

   
    
    
}
